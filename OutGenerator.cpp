#include <iostream>
#include <string>
#include <random>
#include <fstream>


int main() {
	
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<int> dis(0, 9), tab(0, 2);

	std::string operations[3] = {"ADD", "SUB", "MUL"};
	std::string * wskOpe = operations;

	char registers = 'R';
	char * regPoint = &registers;

	std::fstream file;
	file.open("RandomRegisters.txt", std::ios::out);
	
	if ( !file.good()) std::cerr << "cant open file " << std::endl;
	
	else {
		for (int n = 0; n < 100; n++) {
			file << wskOpe[tab(gen)] << " ";
			file << *regPoint << dis(gen) << " ";
			file << *regPoint << dis(gen) << " ";
			file << *regPoint << dis(gen) << std::endl;
		}
	}
	file.close();

	return 0;
}